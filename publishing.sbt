publishMavenStyle := true
publishTo         := Some("GitLab Packages" at "https://gitlab.com/api/v4/projects/11182177/packages/maven")

onLoad in Global := (onLoad in Global).value andThen { state =>
  import org.apache.ivy.util.url._

  val urlHandlerDispatcher = new URLHandlerDispatcher {
    super.setDownloader("http", GitLabUrlHandler)
    super.setDownloader("https", GitLabUrlHandler)

    override def setDownloader(protocol: String, downloader: URLHandler): Unit = {
      if (!(protocol == "http" || protocol == "https")) {
        super.setDownloader(protocol, downloader)
      }
    }
  }

  URLHandlerRegistry.setDefault(urlHandlerDispatcher)

  state
}
