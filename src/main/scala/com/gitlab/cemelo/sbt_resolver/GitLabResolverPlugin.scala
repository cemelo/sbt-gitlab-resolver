package com.gitlab.cemelo.sbt_resolver

import sbt._
import sbt.Keys._

object GitLabResolverPlugin extends AutoPlugin {

  override def trigger = allRequirements

  override def projectSettings: Seq[Def.Setting[_]] = Seq(onLoad in Global := (onLoad in Global).value andThen {
    state =>
      import org.apache.ivy.util.url._

      val urlHandlerDispatcher: URLHandlerDispatcher = new URLHandlerDispatcher {
        val handler = new GitLabUrlHandler
        super.setDownloader("http", handler)
        super.setDownloader("https", handler)

        override def setDownloader(protocol: String, downloader: URLHandler): Unit = {
          if (!(protocol == "http" || protocol == "https")) {
            super.setDownloader(protocol, downloader)
          }
        }
      }

      URLHandlerRegistry.setDefault(urlHandlerDispatcher)

      state
  })
}
