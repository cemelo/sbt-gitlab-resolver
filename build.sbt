name         := "sbt-gitlab-resolver"
organization := "com.gitlab.cemelo"
version      := "1.1.0"

description := "GitLab Packages Resolver"

scalaVersion         := "2.12.8"
sbtVersion in Global := "1.2.8"
sbtPlugin            := true
