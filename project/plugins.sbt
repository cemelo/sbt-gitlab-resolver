addSbtPlugin("com.geirsson"            % "sbt-scalafmt" % "1.5.1")
addSbtPlugin("io.get-coursier"         % "sbt-coursier" % "1.0.3")
addSbtPlugin("com.softwaremill.clippy" % "plugin-sbt"   % "0.5.3")
