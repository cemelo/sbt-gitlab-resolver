import java.io.File
import java.net.URL
import scala.util.Try
import com.typesafe.config.ConfigFactory
import okhttp3.{MediaType, OkHttpClient, Request, RequestBody}
import org.apache.ivy.util.{CopyProgressEvent, CopyProgressListener}
import org.apache.ivy.util.url.IvyAuthenticator
import sbt._
import sbt.internal.librarymanagement.ivyint.{ErrorMessageAuthenticator, GigahorseUrlHandler}

object GitLabUrlHandler extends GigahorseUrlHandler {

    private lazy val okHttpClient: OkHttpClient = {
      GigahorseUrlHandler.http
        .underlying[OkHttpClient]
        .newBuilder()
        .authenticator(new sbt.internal.librarymanagement.JavaNetAuthenticator)
        .followRedirects(true)
        .followSslRedirects(true)
        .build
    }

    private val EmptyBuffer: Array[Byte] = new Array[Byte](0)

    override def upload(source: File, dest0: URL, l: CopyProgressListener): Unit = {

      if (("http" != dest0.getProtocol) && ("https" != dest0.getProtocol)) {
        throw new UnsupportedOperationException("URL repository only support HTTP PUT at the moment")
      }

      IvyAuthenticator.install()
      ErrorMessageAuthenticator.install()

      val dest = normalizeToURL(dest0)

      val body = RequestBody.create(MediaType.parse("application/octet-stream"), source)

      val request = sys.env.get("CI_JOB_TOKEN") match {
        case Some(ciToken) if dest.getHost.endsWith("gitlab.com") =>
          new Request.Builder()
            .url(dest)
            .addHeader("Job-Token", ciToken)
            .put(body)
            .build()

        case None if dest.getHost.endsWith("gitlab.com") =>
          val privateToken: String = sys.env.getOrElse("GITLAB_PRIVATE_TOKEN", Try {
            // Find credentials file under .sbt
            val credentialsFile = file(sys.props("user.home")) / ".sbt" / "gitlab.conf"
            val config          = ConfigFactory.parseFile(credentialsFile)

            config.getString("gitlab.private-token")
          }.getOrElse(""))

          new Request.Builder()
            .url(dest)
            .addHeader("Private-Token", privateToken)
            .put(body)
            .build()

        case _ =>
          new Request.Builder()
            .url(dest)
            .put(body)
            .build()
      }

      if (l != null) {
        l.start(new CopyProgressEvent())
      }
      val response = okHttpClient.newCall(request).execute()
      try {
        if (l != null) {
          l.end(new CopyProgressEvent(EmptyBuffer, source.length()))
        }
        validatePutStatusCode(dest, response.code(), response.message())
      } finally {
        response.close()
      }
    }
  }
